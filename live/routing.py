from django.urls import re_path

from live import Consumer

websocket_urlpatterns=[
    re_path('(?P<room_name>\w+)/(?P<person_name>\w+)/$',Consumer.Consumer)
]
